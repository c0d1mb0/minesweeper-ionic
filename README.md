# Minesweeper
Simple Minesweeper game in Ionic

## Prerequisites
The only prerequisite is bower:

	npm install -g bower

## Installation
Execute the following commands to install the application:

	npm install
	bower install
	ionic state restore
	ionic config build
	ionic build ios
	ionic build android

## Run
Execute the following commands to run the application on iOS or Android:

	ionic emulate ios
	ionic emulare android